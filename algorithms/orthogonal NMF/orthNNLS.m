% OrthNNLS solves 
% 
% min_{H >= 0 and HH^T is a diagonal matrix} ||X-WH||_F^2 
% 
% see F. Pompili, N. Gillis, P.-A. Absil and F. Glineur, "Two Algorithms for
% Orthogonal Nonnegative Matrix Factorization with Application to
% Clustering", Neurocomputing 141, pp. 15-25, 2014.

function [H,norm2v] = orthNNLS(X,W) 

[m,n] = size(X); 
[m,r] = size(W); 
% Normalize columns of W
norm2w = sqrt(sum(W.^2,1));
Wn = W.*repmat(1./(norm2w+1e-16),m,1);
A = X'*Wn; % n by r matrix of "angles" between the columns of W and X
[a,b] = max(A'); % best column of W to approx. each column of X
H = zeros(r,n); 
% Assign the optimal weights to H(b(i),i) > 0
for k = 1 : r
    Kk{k} = find(b==k); 
    sizeclustk(k) = length(Kk{k}); 
    H(k,Kk{k}) = Wn(:,k)'*X(:,Kk{k})/norm2w(k);
end
% Deal with empty clusters (happens relatively rarely) 
emptyclust = find(sizeclustk == 0); 
for k = 1 : length(emptyclust)
    % split largest cluster in two using ONMF itself  
    [maxclus,indmax] = max(sizeclustk); 
    warning('Empty cluster --> largest cluster split in two'); 
    optionssplit.display = 0; 
    [Ws,Hs] = alternatingONMF(X(:,Kk{indmax}),2,optionssplit); 
    % First cluster
    W(:,indmax) = Ws(:,1); 
    H(indmax,Kk{indmax}) = Hs(1,:); 
    % Second cluster 
    W(:,emptyclust(k)) = Ws(:,2); 
    H(emptyclust(k),Kk{indmax}) = Hs(2,:); 
    % Update clusters 
    Kk{indmax} = find(H(indmax,:)>0); 
    sizeclustk(indmax) = length(Kk{indmax}); 
    Kk{emptyclust(k)} = find(H(emptyclust(k),:)>0); 
    sizeclustk(emptyclust(k)) = length(Kk{emptyclust(k)}); 
end    