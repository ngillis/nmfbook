function [k,normM,U] = SPArefine(M,K)

% This code solved the following problem: 
% 
% Given the matrix M (m-by-n) and the index set K in {1,2,...n}, 
% it provides the index k that maximizes the volumne of conv(M(:,[K,k])). 
% 
% 
% *** Description ***
% At each step of the algorithm, the column of M maximizing ||.||_2 is 
% extracted, and M is updated by projecting its columns onto the orthogonal 
% complement of the extracted column. 
%
% See N. Gillis and S.A. Vavasis, Fast and Robust Recursive Algorithms 
% for Separable Nonnegative Matrix Factorization,  IEEE Trans. on Pattern 
% Analysis and Machine Intelligence 36 (4), pp. 698-714, 2014.
% 
% [k,normM,U] = SPArefine(M,K) 
%
% ****** Input ******
% M = WH + N : a (normalized) noisy separable matrix, that is, W is full rank, 
%              H = [I,H']P where I is the identity matrix, H'>= 0 and its 
%              columns sum to at most one, P is a permutation matrix, and
%              N is sufficiently small. 
% K          : set of indices in {1,2,...,n}
%
% ****** Output ******
% k        : next index to maximize the volume of conv(W(:,[K, k])) 
% normM    : the l2-norm of the columns of the last residual matrix. 
% U        : normalized extracted columns of the residual. 
%
% --> normM and U can be used to continue the recursion later on without 
%     recomputing everything from scratch. 
%
% This implementation of the algorithm is based on the formula 
% ||(I-uu^T)v||^2 = ||v||^2 - (u^T v)^2. 

[m,n] = size(M); 

normM = sum(M.^2); 
normM1 = normM; 

% Compute residual norms using successive projections 
for i = 1 : length(K); 
    U(:,i) = M(:,K(i)); 
    % Compute (I-u_{i-1}u_{i-1}^T)...(I-u_1u_1^T) U(:,i), that is, 
    % R^(i)(:,J(i)), where R^(i) is the ith residual (with R^(1) = M).
    for j = 1 : i-1
        U(:,i) = U(:,i) - U(:,j)*(U(:,j)'*U(:,i));
    end
    % Normalize U(:,i)
    U(:,i) = U(:,i)/norm(U(:,i)); 
    % Update the norm of the columns of M after orhogonal projection using
    % the formula ||r^(i)_k||^2 = ||r^(i-1)_k||^2 - ( U(:,i)^T m_k )^2 for all k. 
    normM = normM - (U(:,i)'*M).^2; 
    i = i + 1; 
end

% Index k to add to K 
[a,b] = max(normM); 
% Check ties up to 1e-6 precision
b = find((a-normM)/a <= 1e-6); 
% In case of a tie, select column with largest norm of the input matrix M 
if length(b) > 1, 
    [c,d] = max(normM1(b)); 
    b = b(d); 
end
k = b; 